# shortlinks

An NGINX container with config files generated from Jinja2 containers to setup a
simple shortlinks system.

Used in [coala][coala] site.

[coala]: https://coala.io
